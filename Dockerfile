ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER

RUN apk update \
&& apk upgrade --available --no-cache \
&& apk add --no-cache --upgrade unbound$APKVER drill openssl tzdata \
&& mkdir -p /var/lib/unbound && chown unbound:unbound /var/lib/unbound

WORKDIR /etc/unbound/unbound.conf.d
WORKDIR /etc/unbound/local.conf.d

WORKDIR /etc/unbound
COPY --chmod=644 conf/unbound.conf .

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh entrypoint.sh ./

CMD [ "entrypoint.sh" ]
VOLUME /etc/unbound/local.conf.d
EXPOSE 53/tcp 53/udp

HEALTHCHECK CMD unbound-control status || exit 1
